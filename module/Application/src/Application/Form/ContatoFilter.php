<?php

namespace Application\Form;

use Zend\InputFilter\InputFilter;

class ContatoFilter extends InputFilter {

	public function __construct() {

		$this->add(array(
			'name' => 'nome',
			'required' => true,
			'filters' => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			),
			'validators' => array(
				array(
					'name' => 'NotEmpty',
				),
				array(
					'name' => 'StringLength',
					'options' => array(
						'min' => 3,
						'max' => 200,
					),
				)
			)
		));
		
		$this->add(array(
			'name' => 'email',
			'required' => true,
			'filters' => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			),
			'validators' => array(
				array(
					'name' => 'NotEmpty',
				),
				array(
					'name' => 'StringLength',
					'options' => array(
						'min' => 3,
						'max' => 200,
					),
				)
			)
		));
		
		$this->add(array(
			'name' => 'telefone',
			'required' => false,
			'filters' => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			),
			'validators' => array(
				array(
					'name' => 'NotEmpty',
				),
				array(
					'name' => 'StringLength',
					'options' => array(
						'min' => 8,
						'max' => 15,
					),
				)
			)
		));
		
		$this->add(array(
			'name' => 'celular',
			'required' => false,
			'filters' => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			),
			'validators' => array(
				array(
					'name' => 'NotEmpty',
				),
				array(
					'name' => 'StringLength',
					'options' => array(
						'min' => 8,
						'max' => 15,
					),
				)
			)
		));
		$this->add(array(
			'name' => 'foto',
			'required' => false,
		));
	}

}
