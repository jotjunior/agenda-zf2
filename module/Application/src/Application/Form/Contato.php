<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Doctrine\ORM\EntityManager;

class Contato extends Form {

	/**
	 * Construtora do formulário
	 * Onde são registrados todos os campos
	 */
	public function __construct(EntityManager $em) {
		parent::__construct('contato');

		$this->setAttribute('method', 'post');
		$this->setInputFilter(new ContatoFilter);
		
		// criando um campo do tipo HIDDEN
		$campoId = new Element\Hidden;
		// definindo os atributos do campo
		$campoId->setName('id');

		// injetando o campo no formulário
		$this->add($campoId);

		// criando umcampo do tipo TEXT
		$campo = new Element\Text;
		$campo->setName('nome');
		$campo->setLabel('Nome: ');
		$campo->setAttributes(array(
			'class' => 'span12',
			'id' => 'contato-nome',
		));
		// injetando o campo no formulário
		$this->add($campo);


		// criando umcampo do tipo TEXT
		$campo = new Element\Text;
		$campo->setName('email');
		$campo->setLabel('E-mail: ');
		$campo->setAttributes(array(
			'class' => 'span12',
			'id' => 'contato-email',
		));
		// injetando o campo no formulário
		$this->add($campo);

		// criando umcampo do tipo TEXT
		$campo = new Element\Text;
		$campo->setName('telefone');
		$campo->setLabel('Telefone: ');
		$campo->setAttributes(array(
			'class' => 'span12',
			'id' => 'contato-telefone',
		));
		// injetando o campo no formulário
		$this->add($campo);

		// criando umcampo do tipo TEXT
		$campo = new Element\Text;
		$campo->setName('celular');
		$campo->setLabel('Celular: ');
		$campo->setAttributes(array(
			'class' => 'span12',
			'id' => 'contato-telefone',
		));
		// injetando o campo no formulário
		$this->add($campo);

		// criando umcampo do tipo TEXT
		$campo = new Element\File;
		$campo->setName('foto');
		$campo->setLabel('Foto: ');
		$campo->setAttributes(array(
			'class' => 'span12',
			'id' => 'contato-foto',
		));
		// injetando o campo no formulário
		$this->add($campo);

		// criando umcampo do tipo TEXT
		$repository = $em->getRepository('Application\Entity\Categoria');
		$listaCategorias = $repository->findAll();
		$lista = array();
		foreach ($listaCategorias as $categoria) {
			$lista[$categoria->getId()] = $categoria->getNome();
		}
		
		$campo = new Element\Select;
		$campo->setName('categoria');
		$campo->setLabel('Categoria: ');
		$campo->setOptions(array(
			'empty_option' => 'Selecione uma categoria',
			'value_options' => $lista
		));
		$campo->setAttributes(array(
			'class' => 'span12',
			'id' => 'contato-foto',
		));
		// injetando o campo no formulário
		$this->add($campo);

		// criando o botão enviar
		$botaoEnviar = new Element\Submit;
		$botaoEnviar->setName('enviar');
		$botaoEnviar->setValue('Enviar');
		$botaoEnviar->setAttribute('class', 'btn btn-success');
		$this->add($botaoEnviar);
	}

}
