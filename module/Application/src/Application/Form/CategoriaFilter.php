<?php

namespace Application\Form;

use Zend\InputFilter\InputFilter;

class CategoriaFilter extends InputFilter {

	public function __construct() {

		$this->add(array(
			'name' => 'nome',
			'required' => true,
			'filters' => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			),
			'validators' => array(
				array(
					'name' => 'NotEmpty',
				),
				array(
					'name' => 'StringLength',
					'options' => array(
						'min' => 2,
						'max' => 50,
					),
				)
			)
		));
	}

}
