<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class Categoria extends Form {

	/**
	 * Construtora do formulário
	 * Onde são registrados todos os campos
	 */
	public function __construct() {
		parent::__construct('categoria-form');

		$this->setAttribute('method', 'post');
		$this->setInputFilter(new CategoriaFilter);
		
		// criando um campo do tipo HIDDEN
		$campoId = new Element\Hidden;
		// definindo os atributos do campo
		$campoId->setName('id');

		// injetando o campo no formulário
		$this->add($campoId);

		// criando umcampo do tipo TEXT
		$campoNome = new Element\Text;
		// definindo os atributos do campo
		$campoNome->setName('nome');
		$campoNome->setLabel('Nome: ');
		$campoNome->setAttributes(array(
			'class' => 'span12',
			'id' => 'categoria-nome',
		));
		// injetando o campo no formulário
		$this->add($campoNome);

		// criando o botão enviar
		$botaoEnviar = new Element\Submit;
		$botaoEnviar->setName('enviar');
		$botaoEnviar->setValue('Enviar');
		$botaoEnviar->setAttribute('class', 'btn btn-success');
		$this->add($botaoEnviar);
	}

}
