<?php

namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;
use BaconQrCode\Renderer\Image\Png as QrCodePNG;
use BaconQrCode\Writer as QrCodeWriter;

class QRCode extends AbstractHelper {

	/**
	 * 
	 * @param string $text
	 * @param int $width
	 * @param int $height
	 * @param boolean $https
	 * @return boolean
	 */
	public function __invoke($text, $width = 80, $height = 80, $force = false) {

		$fileName = 'public/uploads/qrcode/' . md5($text) . '.png';

		if ($force || !is_file($fileName)) {
			$qr = new QrCodeWriter((new QrCodePNG)->setWidth($width)->setHeight($height));
			$qr->writeFile($text, $fileName);
		}

		return '<img src="' . str_replace('public', '', $fileName) . '" width="' . $width . '" height="' . $height . '" />';
	}

}

