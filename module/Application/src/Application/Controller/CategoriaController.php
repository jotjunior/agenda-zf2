<?php

/**
 * CATEGORIAS
 * Controla a criação e exibição das categorias
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Stdlib\Hydrator;
// controla a paginacao das listas
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
// para gerar um PDF
use DOMPDFModule\View\Model\PdfModel;
// para gerar um QRCODE
use BaconQrCode\Renderer\Image\Png as QrCodePNG;
use BaconQrCode\Writer as QrCodeWriter;

class CategoriaController extends AbstractActionController {

	/**
	 * Entity Manager
	 * @var \Doctrine\ORM\EntityManager
	 */
	protected $em;

	/**
	 * Página principal das categorias
	 * @return \Zend\View\Model\ViewModel
	 */
	public function indexAction() {
		// guarda uma instância do repositório, que é o objeto do Doctrine
		// que controla o acesso aos dados do banco
		$repository = $this->getEm()->getRepository('Application\Entity\Categoria');

		// a partir do repositório, faz uma busca na entidade para listar 
		// todas as categorias existentes
		$listaCategorias = $repository->findAll();
		
		// guardando o número da página atual
		$pagina = $this->params()->fromRoute('pagina', 5);

		// instanciando o Zend Paginator
		$paginator = new Paginator(new ArrayAdapter($listaCategorias));
		$paginator->setCurrentPageNumber($pagina);
		$paginator->setDefaultItemCountPerPage(5);

		// injeta a lista de categorias na View
		return new ViewModel(array('categorias' => $paginator));
	}

	/**
	 * Adiciona uma nova categoria no banco de daos
	 * @return \Zend\View\Model\ViewModel
	 */
	public function adicionarAction() {
		// instanciando o formulário de categorias
		$form = new \Application\Form\Categoria;

		$request = $this->getRequest();

		// verifica se foi enviado algum dado por POST
		if ($request->isPost()) {

			// injeta os dados recebidos do POST no formulário
			$form->setData($request->getPost());

			// valida os dados enviados
			if ($form->isValid()) {

				// instanciando a entidade da categoria e a hidratando com os dados do formulário
				$categoria = new \Application\Entity\Categoria($form->getData());

				// persistindo os dados no banco
				$this->getEm()->persist($categoria);

				// finalizando a gravação dos dados
				$this->getEm()->flush();

				// redirecionando o usuário para a lista de categorias
				return $this->redirect()->toRoute('application/list', array('controller' => 'categoria'));
			}
		}

		return new ViewModel(array('form' => $form));
	}

	/**
	 * Edita e altera uma categoria
	 * @return \Zend\View\Model\ViewModel
	 */
	public function alterarAction() {
		// instanciando o formulário de categorias
		$form = new \Application\Form\Categoria;

		// instanciando o repositório para capturar os dados da categoria a ser editada
		$repository = $this->getEm()->getRepository('Application\Entity\Categoria');

		// buscando no banco de dados a categoria selecionada
		$categoria = $repository->find($this->params()->fromRoute('id', 0));

		// injetando os dados da categoria no formulário
		$form->setData($categoria->toArray());

		$request = $this->getRequest();

		// verifica se foi enviado algum dado por POST
		if ($request->isPost()) {

			// injeta os dados recebidos do POST no formulário
			$form->setData($request->getPost());

			// valida os dados enviados
			if ($form->isValid()) {
				// Hidratando a categoria com os dados do formulario
				(new Hydrator\ClassMethods)->hydrate($form->getData(), $categoria);

				// persistindo os dados no banco
				$this->getEm()->persist($categoria);

				// finalizando a gravação dos dados
				$this->getEm()->flush();

				// redirecionando o usuário para a lista de categorias
				return $this->redirect()->toRoute('application/list', array('controller' => 'categoria'));
			}
		}

		return new ViewModel(array('form' => $form, 'id' => $this->params()->fromRoute('id')));
	}

	/**
	 * Removendo um registro do banco de dados
	 * @return \Zend\View\Model\ViewModel
	 */
	public function removerAction() {

		// buscando uma referênca da entidade
		$categoria = $this->getEm()->getReference('Application\Entity\Categoria', $this->params()->fromRoute('id', 0));

		// removendo a entidade
		$this->getEm()->remove($categoria);

		// finalizando a remoção
		$this->getEm()->flush();

		// redirecionando o usuário para a lista de categorias
		return $this->redirect()->toRoute('application/list', array('controller' => 'categoria'));
	}

	/**
	 * Gera um arquivo PDF com o conteúdo da View
	 * @return \DOMPDFModule\View\Model\PdfModel
	 */
	public function gerarPdfAction() {
		// monta a lista de categorias
		$repository = $this->getEm()->getRepository('Application\Entity\Categoria');
		$listaCategorias = $repository->findAll();

		// gera o PDF
		$pdf = new PdfModel();
		$pdf->setOption('filename', 'categorias');
		$pdf->setOption('enable_remote', true);
		$pdf->setOption('paperSize', 'a4');
		$pdf->setVariables(array('categorias' => $listaCategorias));

		return $pdf;
	}

	public function qrCodeAction() {
		$qr = new QrCodeWriter((new QrCodePNG)->setWidth(100)->setHeight(100));
		echo $qr->writeString('batatinha');
		exit();
	}

	/**
	 * Método auxiliar que guarda o EntityManager do Doctrine
	 * @return type
	 */
	protected function getEm() {
		// se o atributo $this->em "ainda" não for uma instância do EntityManager
		// atribui o objeto à esta variável
		if (!($this->em instanceof \Doctrine\ORM\EntityManager)) {
			// localiza o serviço EntityManager do Doctrine 
			$this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		}

		// retorna o EntityManager
		return $this->em;
	}

}
