<?php

/**
 * CONTATOS
 * Controla a criação e exibição das contatos
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Stdlib\Hydrator;
// controla a paginacao das listas
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
// para gerar um PDF
use DOMPDFModule\View\Model\PdfModel;
// para gerar um QRCODE
use BaconQrCode\Renderer\Image\Png as QrCodePNG;
use BaconQrCode\Writer as QrCodeWriter;

class ContatoController extends AbstractActionController {

	protected $em;

	public function indexAction() {

		$repository = $this->getEm()->getRepository('Application\Entity\Contato');

		$listaContatos = $repository->findAll();

		// guardando o número da página atual
		$pagina = $this->params()->fromRoute('pagina', 5);

		// instanciando o Zend Paginator
		$paginator = new Paginator(new ArrayAdapter($listaContatos));
		$paginator->setCurrentPageNumber($pagina);
		$paginator->setDefaultItemCountPerPage(5);

		return new ViewModel(array('contatos' => $paginator));
	}

	public function alterarAction() {
		$form = new \Application\Form\Contato($this->getEm());

		$request = $this->getRequest();

		$repository = $this->getEm()->getRepository('Application\Entity\Contato');
		$contato = $repository->find($this->params()->fromRoute('id', 0));
		
		$form->setData($contato->toArray());

		if ($request->isPost()) {

			$post = $request->getPost()->toArray();
			$file = $request->getFiles()->toArray();
			$data = array_merge_recursive($post, $file);

			$form->setData($data);

			if ($form->isValid()) {

				if ($data['foto']['name'] == '') {
					unset($data['foto']);
				}
				(new Hydrator\ClassMethods)->hydrate($data, $contato);

				$categoria = $this->getEm()->getReference('Application\Entity\Categoria', $form->get('categoria')->getValue());
				$contato->setCategoria($categoria);

				if (isset($data['foto']['name'])) {
					$contato->setFoto($file['foto']['name']);
				}
				$this->getEm()->persist($contato);

				$this->getEm()->flush();

				if (isset($data['foto']['name'])) {
					// copiar a foto para o diretorio
					move_uploaded_file($file['foto']['tmp_name'], './public/uploads/fotos/' . $file['foto']['name']);
				}

				$this->redirect()->toRoute('application/list', array('controller' => 'contato'));
			}
		}

		return new ViewModel(array('form' => $form, 'id' => $contato->getId()));
	}

	public function adicionarAction() {
		$form = new \Application\Form\Contato($this->getEm());

		$request = $this->getRequest();

		if ($request->isPost()) {

			$post = $request->getPost()->toArray();
			$file = $request->getFiles()->toArray();
			$data = array_merge_recursive($post, $file);

			$form->setData($data);

			if ($form->isValid()) {

				$contato = new \Application\Entity\Contato($form->getData());
				$categoria = $this->getEm()->getReference('Application\Entity\Categoria', $form->get('categoria')->getValue());
				$contato->setCategoria($categoria);
				$contato->setFoto($file['foto']['name']);

				$this->getEm()->persist($contato);

				$this->getEm()->flush();

				// copiar a foto para o diretorio
				move_uploaded_file($file['foto']['tmp_name'], './public/uploads/fotos/' . $file['foto']['name']);

				$this->redirect()->toRoute('application/list', array('controller' => 'contato'));
			}
		}

		return new ViewModel(array('form' => $form));
	}

	public function removerAction() {

		// buscando uma referênca da entidade
		$contato = $this->getEm()->getReference('Application\Entity\Contato', $this->params()->fromRoute('id', 0));

		// removendo a entidade
		$this->getEm()->remove($contato);

		// finalizando a remoção
		$this->getEm()->flush();

		// redirecionando o usuário para a lista de categorias
		return $this->redirect()->toRoute('application/list', array('controller' => 'contato'));
	}	
	
	protected function getEm() {
		// se o atributo $this->em "ainda" não for uma instância do EntityManager
		// atribui o objeto à esta variável
		if (!($this->em instanceof \Doctrine\ORM\EntityManager)) {
			// localiza o serviço EntityManager do Doctrine 
			$this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		}

		// retorna o EntityManager
		return $this->em;
	}

}
