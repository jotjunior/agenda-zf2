<?php
/**
 * Índice do sistema.
 * Primeira página que será procurada quando o sistema for carregado 
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {

	public function indexAction() {
		
		$this->layout('layout/secundario');
		return new ViewModel();
	}
	
	public function testeAction() {
		$variavel = "Salesiano";
		
		return new ViewModel(array('escola' => $variavel));
	}

}
