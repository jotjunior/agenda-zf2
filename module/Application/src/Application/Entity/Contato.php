<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * Contatos
 *
 * @ORM\Table(name="contatos", indexes={@ORM\Index(name="fk_contatos_categorias_idx", columns={"categoria_id"})})
 * @ORM\Entity
 */
class Contato {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nome", type="string", length=255, nullable=false)
	 */
	private $nome;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255, nullable=true)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="telefone", type="string", length=15, nullable=true)
	 */
	private $telefone;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="celular", type="string", length=15, nullable=true)
	 */
	private $celular;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="nascimento", type="date", nullable=true)
	 */
	private $nascimento;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="foto", type="string", length=255, nullable=true)
	 */
	private $foto;

	/**
	 * @var \Categorias
	 *
	 * @ORM\ManyToOne(targetEntity="Categoria")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
	 * })
	 */
	private $categoria;

	public function __construct($options = null) {
		(new Hydrator\ClassMethods)->hydrate($options, $this);
	}

	public function toArray() {
		return array(
			'id' => $this->id,
			'nome' => $this->nome,
			'email' => $this->email,
			'telefone' => $this->telefone,
			'celular' => $this->celular,
			'nascimento' => ($this->nascimento) ? $this->nascimento->format('d/m/Y') : null,
			'foto' => $this->foto,
			'categoria' => $this->categoria->getId()
		);
	}

	public function __toString() {
		return $this->nome;
	}

	public function getId() {
		return $this->id;
	}

	public function getNome() {
		return $this->nome;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getTelefone() {
		return $this->telefone;
	}

	public function getCelular() {
		return $this->celular;
	}

	public function getNascimento() {
		return $this->nascimento;
	}

	public function getFoto() {
		return $this->foto;
	}

	public function getCategoria() {
		return $this->categoria;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}

	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	public function setTelefone($telefone) {
		$this->telefone = $telefone;
		return $this;
	}

	public function setCelular($celular) {
		$this->celular = $celular;
		return $this;
	}

	public function setNascimento($nascimento, $format = "d/m/Y") {
		$this->nascimento = \DateTime::createFromFormat($format, $nascimento);
		return $this;
	}

	public function setFoto($foto) {
		$this->foto = $foto;
		return $this;
	}

	public function setCategoria($categoria) {
		$this->categoria = $categoria;
		return $this;
	}

}
