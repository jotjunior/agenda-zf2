<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * Categorias
 *
 * @ORM\Table(name="categorias")
 * @ORM\Entity
 */
class Categoria {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nome", type="string", length=55, nullable=false)
	 */
	private $nome;

	/**
	 * Construtora da classe. Neste caso, responsável por "hidratar" o objeto
	 * com os dados vindos da aplicação
	 * @param array $options
	 */
	public function __construct($options = null) {
		(new Hydrator\ClassMethods)->hydrate($options, $this);
	}
	
	/**
	 * Classe mágica do PHP que retorna uma string quando se tenta retornar o 
	 * objeto sem informar nenhum método
	 * @return string|null
	 */
	public function __toString() {
		return $this->nome;
	}
	
	/**
	 * Converte os atributos da classe em um array
	 */
	public function toArray() {
		return (new Hydrator\ClassMethods)->extract($this);
	}
	
	public function getId() {
		return $this->id;
	}

	public function getNome() {
		return $this->nome;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}

}
